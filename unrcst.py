#!/usr/bin/python

import sys
import getopt
from tree import parse,Tree
from tree import Tree,randomTree
from defs import *
from utree import UnrootedTree

import os

def costsmp(cnt,ct,s,u):
    print u.smprepr(s,ct)
   
def pairing(l1,l2,fun,sequential,ct):
    if sequential:
        for ind,p in enumerate(zip(l1,l2)):
            fun("%d" % (ind+1),ct,*p)
    else:
        cnt=1
        for L1 in l1:
            for L2 in l2:
                fun("%d" % cnt,ct,L1,L2)
                cnt+=1
                    
__doc__="""
Usage:
    -g TREE defines a gene tree
    -s TREE defines a species tree
"""

from optparse import OptionParser

def main():
    usage = """usage: %prog [-g FILE|-] [-s FILE|-] [inputfile1 inputfile2 ...]
Input. Stdin is read in case of no input files or no trees defined in -s/-g. 
- denotes reading from stdin.
Output. Nodes are decorated with atributes: 
cost=NUM (total cost of rooting placed on the edge),
top=NUM (direction of edge: 0=empty ---, -1=single <---, 1=single --->, 2=double <--->)

RF: default
unrcst.py -g '(a,(b,c))' -s '(a,(b,c))' 
((a top=0 cost=0,b top=-1 cost=2) top=1 cost=2,c top=-1 cost=2)

DC: 
unrcst.py -g '(a,(b,c))' -s '(a,(b,c))' -c DC
((b top=-1 cost=5,c top=-1 cost=5) top=0 cost=4,a top=0 cost=4)

""" 

    parser = OptionParser(usage)

    parser.add_option("-s", "--speciesgraph", action="store",
                  help="the set of species trees(s): read from a file, a string or - (stdin)")

    parser.add_option("-g", "--genetree", action="store",
                  help="the set of gene tree(s): read from a file, a string or - (stdin)")

    parser.add_option("-c","--cost", action="store",
                     help="type of cost: types: RF (default), DC")

    (options, args) = parser.parse_args()

    from utils import Env

    env=Env()
    import defs

    def itgpair(num,d,ts,tg):
        vp(PRDET,"# %d gene tree-species graph" % num)
        if 'g' in options.evaluate: pt(tg)
        else: vp(PRDET,"#",tg)
        if 's' in options.evaluate: pt(ts)
        else: vp(PRDET,"#",ts)

    printenv=0

    if options.speciesgraph: env.readfs(options.speciesgraph,"s")
    if options.genetree: env.readfs(options.genetree,"g")

    if args:
        for a in args:
            env.readfs(a,"gs")
    elif not (options.speciesgraph or options.genetree):
            env.readfs("-","gs")

    if printenv: env.printenv()


    if not options.cost: cost="RF"
    else: cost=options.cost

    
    pairing( (Tree(s) for s in env.s), 
             (UnrootedTree(g) for g in env.g), costsmp,0,cost)

if __name__ == "__main__":
    main()



