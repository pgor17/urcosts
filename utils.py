

import os
import sys
import random

from defs import  *

class Env:
    def __init__(self):
        self.g=[]
        self.s=[]
        self.t=[]
        self.df={}
        self.subdf={}
        self.macros={}
        self.df["flatten"]=0.001

    def get(self,n):
        df=dict(self.df)
        df.update(self.subdf.get(n,{}))
        return df

    def printvar(self,var,val,pref=None):
        if pref: print "%s.%s=" % (pref,var),
        else: print "%s=" % var,
        if type(val)==str:
            print "'%s'" % val
        else: print val

    def printenv(self):
        print "# Main"
        for i in self.df: self.printvar(i,self.df[i])
        print "# Subf"
        for k in self.subdf:
            for i in self.subdf[k]:
                self.printvar(i,self.subdf[k][i],k)

    def readconfig(self,s):
        if s=="-": s=sys.stdin.read()
        elif os.path.exists(s): s=open(s).read()
        else: s=s.replace(";","\n")
     
        self._reads(s,"-")
            

    def readfs(self,s,typestr="gst"):
        if s=="-": s=sys.stdin.read()
        elif os.path.exists(s): s=open(s).read()
        self._reads(s,typestr)

    def macro(self,s):
        t=s.split("&")
        #print t
        res=t[0]
        for i,k in enumerate(t):
            if i==0: continue
            if "(" not in k: 
                res+=k
                continue
            com,rest=k.split("(",1)

            if com in ["lt","rt","bin","randbin","randmul"]:
                p,rest=k[len(com)+1:].split(")",1)
                p=p.split(",")

                # expanding
                r=[]
                for k in p:
                    #print k
                    if len(k)>=3 and k[1]=="-":
                    # interval
                        #print "int",k[3:].isdigit()
                        nums=1
                        if len(k)>3:
                            if k[3:].isdigit(): nums=int(k[3:])
                            else: continue
                        for i in xrange(nums):
                            for c in xrange(ord(k[0]),ord(k[2])+1): 
                                r.append(chr(c)+(("%d"%i) if i else ""))
                p=r
                if not p: continue   

                #print p

                if com=="randbin":                
                    while len(p)>1:                       
                        r=[]
                        cstr=""
                        a=p.pop(random.randint(0,len(p)-1))
                        b=p.pop(random.randint(0,len(p)-1))
                        p.append("("+a+","+b+")")
                    rs=p[0]

                if com=="randmul":      
                    #print p
                    while len(p)>1:                       
                        r=[]
                        cstr=""
                        if len(p)>2:
                            deg=random.randint(2,len(p)-1)
                        else: deg=2
                        #print deg,p
                        cs=""
                        while p and deg:
                            cs+=("," if cs else "") + p.pop(random.randint(0,len(p)-1))
                            deg-=1
                        p.append("("+cs+")")
                    rs=p[0]
                    #print rs
                    
                    
                if com=="bin":                    
                    while len(p)>1:                       
                        r=[]
                        cstr=""
                        for i,ps in enumerate(p):
                            if i%2:
                                r.append("("+cstr+","+ps+")")
                                cstr=""
                            else: cstr=ps
                        if cstr: r.append(cstr)
                        p=r
                    rs=p[0]
                elif com=="lt":                    
                    rs=p[0]  
                    for j in p[1:]: rs="("+rs+","+j+")"
                elif com=="rt":
                    p.reverse()
                    rs=p[0]  
                    for j in p[1:]:
                        rs="("+j+","+rs+")"

                res+=rs+rest
                continue
            else:
                print "Warning: macro &%s not recognized (ignored)" % com
            
            res+=k
        #print "####",res
        s=res
        for k,v in self.macros.items():  
            s=s.replace("&"+k,v)
        return s

    def _reads(self,s,typestr):
        pos=0
        res=[]
        prev=''
        for i in s.split("\n"):
            if prev:  i=prev+i.strip()
            if not i: continue
            if i[-1]=='\\': 
                prev=i[:-1]
                continue

            ct=typestr[pos]            
            if i[0]=='#': 
                ts=i[1:].split("=")
                if len(ts)==2 and ts[0].isalpha():                    
                    self.macros[ts[0]]=self.macro(ts[1])
                continue

            #if i[0:6]=='import':
            #    i=i[6:].strip()
            #    print i
            #    continue
                

            if i[0]=="&" and len(i)>1 and i[1] in "gts":
                ct=i[1]
                i=i[2:].strip()
            else:
                t=i.split("=",1)
                if len(t)==2 and t[0].strip().replace('.','x').isalpha():
                    # get dots
                    var=t[0].strip()
                    vt=var.split(".")
                    if len(vt)==1:
                    # caution with eval
                        d=self.df
                    elif len(vt)==2:
                        if vt[0] not in self.subdf:
                            self.subdf[vt[0]]={}
                        d=self.subdf[vt[0]]
                        var=vt[1]
                    else:
                        raise Exception("Too many dots in %s",t)
                    try:
                        d[var]=eval(t[1],globals(),locals())
                    except Exception,e:
                        print t[1]
                        print e
                        sys.exit(-1)
                        
                    continue

            if ct=="-": 
                print "Unknown assignment?",i[0]
                sys.exit(-1)

            if ct=='t': self.t.append(self.macro(i))
            elif ct=='s': self.s.append(self.macro(i))
            elif ct=='g': self.g.append(self.macro(i))
            elif ct=='-': pass
            else: 
                print "Unknown tree type: %s",ct
                sys.exit(-1)
            pos+=1
            if pos>=len(typestr): pos=0
            prev=''



def savedf(df):
    return "\n".join("%s=%s" % (k,v) for k,v in df.items())
        
        
        
    
