# Comparing unrooted gene tree with a rooted species tree under RF and DC.

Try:



```text
unrcst.py --help


Usage: unrcst.py [-g FILE|-] [-s FILE|-] [inputfile1 inputfile2 ...]

Input. Stdin is read in case of no input files or no trees defined in -s/-g. - denotes reading from stdin.

Output. Nodes are decorated with atributes: 
cost=NUM (total cost of rooting placed on the edge),
top=NUM (direction of edge: 0=empty ---, -1=single <---, 1=single --->, 2=double <--->)

RF: default
unrcst.py -g '(a,(b,c))' -s '(a,(b,c))' 
((a top=0 cost=0,b top=-1 cost=2) top=1 cost=2,c top=-1 cost=2)

DC: 
unrcst.py -g '(a,(b,c))' -s '(a,(b,c))' -c DC
((b top=-1 cost=5,c top=-1 cost=5) top=0 cost=4,a top=0 cost=4)

Options:
  -h, --help            show this help message and exit
  -s SPECIESGRAPH, --speciesgraph=SPECIESGRAPH
                        the set of species trees(s): read from a file, a
                        string or - (stdin)
  -g GENETREE, --genetree=GENETREE
                        the set of gene tree(s): read from a file, a string or
                        - (stdin)
  -c COST, --cost=COST  type of cost: types: RF (default), DC

```
 
[1] Górecki P., Eulenstein O. (2012) Deep Coalescence Reconciliation with Unrooted Gene Trees: Linear Time Algorithms. In: Gudmundsson J., Mestre J., Viglas T. (eds) Computing and Combinatorics. COCOON 2012. Lecture Notes in Computer Science, vol 7434. Springer, Berlin, Heidelberg
 
https://doi.org/10.1007/978-3-642-32241-9_45
 
[2] Górecki P., Eulenstein O. (2012) A Robinson-Foulds Measure to Compare Unrooted Trees with Rooted Trees. In: Bleris L., Măndoiu I., Schwartz R., Wang J. (eds) Bioinformatics Research and Applications. ISBRA 2012. Lecture Notes in Computer Science, vol 7292. Springer, Berlin, Heidelberg
 
https://doi.org/10.1007/978-3-642-30191-9_12