
from itertools import chain
import tokenize, StringIO
from htlib import _tostr, sindex, pstack

import random
from defs import *
from math import sqrt,pow,log

import re

class TreeNode:
    treenodenum=1
    def __init__(self, *args, **kwargs):
        self.c = args
        for c in self.c: c.parent=self
        self.parent=None
        self.stype = TREENODE
        self.labels=[]
        self.nnum=TreeNode.treenodenum
        TreeNode.treenodenum+=1
        self.branchlendef=False 
        for k in kwargs: setattr(self,k,kwargs[k])
        if not self.branchlendef:
            self.branchlen = 1.0

    # a list of leaves
    def leaves(self):
        if self.c: return list(chain.from_iterable([ l.leaves() for l in self.c]))
        return [self]

    def leaf(self):
        return not self.c

    def smp(self):
       if self.leaf():
          for l in self.labels:
             if type(l)!=tuple: return l
          return ""
       return "("+",".join((c.smp() for c in self.c))+")"
                
    # quite ugly, generator of nodes
    def nodes(self,tp=PREFIX|ALLC|ALLT):
        if self.leaf() and tp & LEAVES and tp & self.stype: yield self
        if not self.leaf():
            if tp & INT and tp & PREFIX and tp & self.stype: yield self
            # special binary case for INFIX
            if tp & INFIX:
                if len(self.c)>2: raise Exception("INFIX require <=2 children")
                for i in self.c[0].nodes(tp): yield i
                yield self
                if len(self.c)>1:
                    for i in self.c[1].nodes(tp): yield i
            else:
                for i in chain.from_iterable((c.nodes(tp) for c in self.c)): yield i
            if tp & INT and tp & POSTFIX and tp & self.stype: yield self



    # used in drawings for showintclusters, showedgelabels
    # @TODO: defining labels nodes
    def smplabel(self): return self.smp()
    

    def setparent(self):
        for n in self.c: n.parent=self

    def isPath(self,n):  # path from self -> ... -> n
        while n:
            if self==n: return True
            n=n.parent
        return False

    def lca(self,n):  # lca between two nodes in a tree
        if self.isPath(n): return self
        if n.isPath(self): return n
        n=n.parent
        while n:
            if n.isPath(self): return n
            n=n.parent
        return None

    # string repr of labels
    def _labels(self,skip=None):
        if not skip: skip=[]
        s=""
        for l in self.labels:
            if type(l)!=tuple or l[0] not in skip:
                s+=_tostr(l)+" "
        return s

    def __str__(self):
         num=""
         lb=self._labels()
         ln=":%f"%self.branchlen if self.branchlendef else ""
         if self.c:
             return "(" + ",".join((c.__str__()  for c in self.c)) + ")"+ln+(" "+lb if lb else "") + num
         return lb+ln+num
    

    def smprepr(self,addlabels=0):
        s=""
        if addlabels: s=" "+self._labels()
        if not self.c: return self.smp()+s
        return "(" + ",".join((c.smprepr(addlabels)  for c in self.c)) + ")"+s

    def __repr__(self):
        return self.__str__()


class Tree:
    num=1
    def __init__(self, root,**kwargs):
        if type(root)==str:
            self.source=root
            root=parse(root)

        self.root=root
        self.nodes=list(root.nodes())
        self.leaves=list(root.leaves())
        for k in kwargs: setattr(self,k,kwargs[k])

        num=0
        for n in self.nodes:
            #n.num=random.randint(1,1000)
            n.num=num
            num+=1
            n.setparent()
        self.num=Tree.num
        Tree.num+=1

    def tree(self,*args): return self

    def nodesgen(self,tp=PREFIX|ALLT|ALLC):
        return self.root.nodes(tp)

    def __repr__(self):
        return self.root.__repr__()

    def __str__(self):
        return self.__repr__()
    

    def add(self,**kwargs):
        for i in kwargs:
            self.root.labels.append((i,kwargs[i]))








        










    



def randomTree(TN=TreeNode,Tr=Tree,**kwargs):

    def genleaves(num):
        cnt=0
        while cnt<num:
            if cnt+ord('a')<=ord('z'): yield chr(cnt+ord('a'))
            else: yield "lf%d" % cnt
            cnt+=1

    minchildren=kwargs.get("minchildren",2)
    maxchildren=kwargs.get("maxchildren",2)
    unique=kwargs.get("unique",False)
    numleaves=kwargs.get("numleaves",100000)
    numspecies=kwargs.get("numspecies",100000)
    s=min(numleaves,numspecies)
    if s==100000:
        raise Exception("numspecies or/and numleaves should be defined")
    if unique: numleaves=numspecies=s
    else:
        if numleaves==100000: numleaves=s
        if numspecies==100000: numspecies=s

    leaves=kwargs.get("leaves",None)
    if not leaves: leaves=genleaves(numspecies)


    cand=[]
    leaves=list(leaves)
    
    for i in xrange(numleaves):
        n=random.randint(0,len(leaves)-1)
        if unique: n=leaves.pop(n)
        else: n=leaves[n]
        cand.append(TN(labels=[n]))

    while len(cand)>1:
        ch=random.randint(minchildren,maxchildren)
        if ch>len(cand): ch=len(cand)
        c=[]
        for i in xrange(ch):
            c.append(cand.pop(random.randint(0,len(cand)-1)))
        cand.append(TN(*c))
    return Tr(cand[0])

# Grammar
#   Node -> Leaf | Internal
#   Leaf -> LAB|STR[:LEN]  -- label is required, def len is 1.0
#   Internal -> (Node,Node,...,Node)[:LEN] [LAB|STR]


def trtokenize(s):
    res=[]    
    i=0
    cur=""
    
    def addc():
        if cur: res.append((tokenize.NAME,cur))
        return ""

    while i<len(s):

        #print "TOKs",s[i:],"CUR=%s" % cur        
           
        if s[i].isspace() or s[i]==";":
            cur=addc()
            i+=1
            continue
        
        if s[i] in "(),^+-~:=":
            cur=addc()
            res.append((None,s[i]))
            i+=1
            continue

        if s[i] in '"\'':
            cur=addc()
            dlm=s[i]
            i+=1
            x=s[i:]
            y=""
#            print "ST",s[i:],dlm
            while True:
#                print "#%s#%s#" % (x,y)
                k=x.find(dlm)
                if k<0: raise Exception('Missing " or \' in %s, pos %d' % (x,i))                        
                if x[k:k+2]=="''":
                    y=y+"''"+x[:k]
                    x=x[k+2:] 
                else: 
                    y=y+x[:k]
                    x=x[k+1:]
                    break
            
            s=x
            i=0
            res.append((tokenize.STRING,y))
            #print res

            continue    
            
        
        if s[i]=="\\" and s[i+1]==' ':
            cur+=" "
            i+=2
            continue
        
        num="-0123456789."
        if s[i] in num and not cur:
            x=""
            while i<len(s) and s[i] in num:
                x+=s[i]
                i+=1
            res.append((tokenize.NUMBER,x))
            continue


        cur=cur+s[i]
        i+=1

    addc()
    res.append((0,""))
    return res
        

def parse(s,TN=TreeNode):

    # tokenize the string, use python tokenizer
    #tokens=list(tokenize.generate_tokens(StringIO.StringIO(s).readline))    
    #print "PYTH",list(tokens)

    tokens=trtokenize(s)

    
    #print "INP",tokens

    def parseVal(t):
        global _nnodecnt
        ctyp,ctok=t[0][0:2]
        del t[0]
        if ctyp==tokenize.STRING: return ctok
        if ctyp==tokenize.NAME: return ctok

        if ctok=="-" and t[0][0]==tokenize.NUMBER:
            ctok=t[0][1]
            del t[0]
            return -float(ctok)

        if ctyp==tokenize.NUMBER: return float(ctok)
        if ctok=="(":
            r=[]
            while t[0][1]!=")":
                r.append(parseVal(t))
                if t[0][1]==',':
                    del t[0]  #eat comma
                    continue
            del t[0] #eat )
            return tuple(r)
        raise Exception("Value (num,label,str or tuple) expected. Found %s." % t[0][1])

    def parseNode(t):
        children=[]
        labels=[]
        blen=1.0
        pos=None
        addattr={}
        blendef=False
        while t[0][1] and t[0][1] not in [',',')']:
            ctyp,ctok=t[0][0:2]
            #print t, "===",t[0],t[1]
            if not t[1]: 
                raise Exception("Parse error: unexpected end of tree string")
                
            if ctyp==tokenize.NAME and t[1][1]=="=":
                lab=ctok
                del t[0:2]
                # tuple, label/string, number
                kr=(lab,parseVal(t))
                labels.append(kr)
                if lab in ["shapewidth"]: addattr[lab]=kr[1]                
                if lab in ["parentpos","lfpos","legendpos","pos"]: addattr[lab]=P(kr[1])
                continue

            if (ctok=="-" and t[1][0]!=tokenize.NUMBER) or ctok in "+^.~": # check -NUM
                del t[0]
                labels.append(ctok)
                continue

            # single val
            if ctyp==tokenize.STRING or ctyp==tokenize.NAME or ctyp==tokenize.NUMBER or ctok in "-":
                labels.append(parseVal(t))
                continue

            # len
            if ctok==':':
                del t[0]
                if t[0][0]==tokenize.NUMBER:
                    blen=float(t[0][1])
                    blendef=True
                    del t[0]
                    continue
                else: raise Exception("Parse error expected number")

            # internal node
            if ctok=='(':
                del t[0] # eat (
                while True:
                    if not t[0][1]:
                        raise Exception("Parse error: unexpected end of tree string (missing ,)?")
                    children.append(parseNode(t))    # new child
                    if t[0][1]==',':
                        del t[0]  #eat comma
                        continue
                    if t[0][1]==')': break
                del t[0]  # eat )
                continue
            if ctok==";":
                del t[0]
                continue
            raise Exception("Parse error expected label, string or :number. Found #%s#" % ctok)

        return TN(*children,labels=labels,branchlen=blen,branchlendef=blendef,**addattr)

    tr=parseNode(tokens)
    if tokens[0][1]: raise Exception("Parse error, symbol left at the end of input string")
    return tr


