#Node Types
HTGENE=1
SPECIATION=2
HTDUP=4
HTLOSS=8
HGTSTART=16
HGTTERM=32
HGTN=HGTSTART|HGTTERM
TREENODE=64
ALLT=127 # by types
HDEBUG=0

#@TODO: other drawing options
BLYPROP=1    # use branch length proportional to y-axis only

TSSMP=1
TSPHYLOGRAM=2 # phylogram tree/shape styles

verbose=0 # verbose level

# 3 - debug

PRPREF=1
PRSMP=2
PRDET=3
PRDEB=4

NONE="none"
SOLID="solid"

def vp(vlevel,*args):
    global verbose
    if vlevel<=verbose:
        for a in args:
            print a,
        print


def pt(t):
    global verbose
    s=None
    
    if hasattr(t,"source"):
        s=t.source

    from htree import HTree
    if isinstance(t,HTree):
        if verbose==PRPREF: print "&t ",
        if s: print s
        else: print t.hstr()
        return
    from dag import HDAG
    if isinstance(t,HDAG):
        if verbose==PRPREF: print "&s ",
        if s: print s
        else: print t
        return

    from tree import Tree
    if isinstance(t,Tree):
        if verbose==PRPREF: print "&g ",
        if s: print s
        else: print t
        return

    print "Unknown tree type to print?"

def ident(x): return x

def gettype(h):
    s=""
    if h & HTGENE: s+="G"
    if h & SPECIATION: s+="S"
    if h & HTDUP: s+="+"
    if h & HTLOSS: s+="-"
    if h & HGTSTART: s+=">"
    if h & HGTTERM: s+="H"
    if h & TREENODE: s+="*"
    return s

# Tree walking constants
LEAVES=128
INT=256
ALLC=LEAVES|INT # by children
PREFIX=512
POSTFIX=1024
INFIX=2048
HGT="->"

def pri(*args):
    for i in args: print i,
    print


def avg(l): return sum(l)/(1.0*len(l))



