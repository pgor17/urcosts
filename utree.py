#!/usr/bin/python
from tree import Tree
from itertools import chain
import sys

class UNode:

    num=1

    def __init__(self,parent=None,label=None):
        self.p=parent
        if parent: parent.p=self
        self.next=None
        self.label=label
        self.num=UNode.num
        UNode.num+=1
        self.c=[]

    def compchildren(self):
        c=self.next
        self.c=[]
        while c!=self:
            self.c.append(c.p)
            c=c.next

    def pdeb(self):
        print self.num,
        if self.p: print self.p.num,
        else: print 'NOP',
        print "%5s" % self.label,
        print " CH=",self.num,

        k=self.next
        while k!=self:
            if k: print k.num,
            else: 
                print "N",
                break
            k=k.next
        print "CHT",
        for c in self.c: print c.num,
        #print self.lflabels(),
        if hasattr(self,"map"): print "M=%s" %self.map,
        if hasattr(self,"rflca"): print "rflca=%d" %self.rflca,
        if hasattr(self,"urf"): print "urf=%d" %self.urf,
        print self,
            
    def psubtree(self):
        if not self.c: return self.label                
        return "(" + ",".join(c.psubtree() for c in self.c)+")"

    def __repr__(self):
        return self.psubtree()

    def leaf(self):
        return not self.c



    def isize(self):
        if self.leaf(): return 0
        return 1+sum(c.isize() for c in self.c)

    def leaves(self):
        if self.c: return list(chain.from_iterable([ l.leaves() for l in self.c]))
        return [self]

    def getrflca(self):
        if self.rflca>=0: return self.rflca
        #print seln,n.num,len(self.map.leaves()),len(self.leaves())
        self.rflca=sum(c.getrflca() for c in self.c) + \
            (1 if len(self.map.leaves())==len(self.leaves()) else 0)
        return self.rflca

    def maxdist(self):
        if self.leaf(): return 0
        return 1+max(c.maxdist() for c in self.c)

    def linfo(self,tree,topmap,rev=1):
        if self.map==tree.root: 
            if self.p.map==tree.root: res=" top=2"
            else: res=" top=%d" % rev
        else:
            if self.p.map==tree.root: res=" top=%d" % -rev
            else: res=" top=0"
        if hasattr(self,"cost"): res+=" cost=%d"%self.cost
        return res

    def lsubtree(self,tree,topmap): 
        if self.leaf(): res=self.label
        else: res="("+",".join([c.lsubtree(tree,topmap) for c in self.c])+")"
        res+=self.linfo(tree,topmap)
        return res
                 

class UnrootedTree:
    
    def __init__(self,tree):
        self.nodes=[]
        self.source=tree
        self.tree=Tree(tree)

        def trav(n,conn=None):
            if n.leaf():
                lf=UNode(conn,n.smplabel())                
                lf._src=n
                lf.next=lf
                return lf            

            first=prev=None
            for c in n.c:
                ucur=UNode()             # children conn
                if not prev: 
                    first=ucur
                else:
                    prev.next=ucur
                trav(c,ucur)      
                prev=ucur
                
            if conn:
                parc=UNode(conn)                
                ucur.next=parc
                parc.next=first
                return parc
            else:
                ucur.next=first
                return ucur
            
       
        self.start=trav(self.tree.root)
        if self.start.next==self.start:
            raise Exception("Tree should have at least 1 node")
            sys.exit(-1)

        if self.start.next.next==self.start:
            # merge (the root has two children)
            a=self.start.p
            b=self.start.next.p
            a.p=b
            b.p=a
            self.start=a
        
        def addn(n):
            n.compchildren()
            self.nodes.append(n)
            for c in n.c: 
                c.p.compchildren()
                self.nodes.append(c.p)                
                addn(c)

        self.nodes=[]
        addn(self.start)
        addn(self.start.p)
            
    def pdeb(self):
        for n in self.nodes: 
            if not n.c: 
                n.pdeb()
                print
        for n in self.nodes: 
            if n.c: 
                n.pdeb()
                print
    
             
    def setmap(self,tree):
        treel=tree.leaves
        for n in self.nodes: 
            n.map=None
            if n.leaf():
                n.map=[ l for l in treel if l.smplabel()==n.label ]
                if len(n.map)!=1:
                    raise Exception("Wrong mapping for %s, found %s" % (n,n.map))
                n.map=n.map[0]
        for n in self.nodes:
            if n.c: 
                n.map=reduce(lambda x,y: x.lca(y), ( c.map for c in n.leaves()))
        n=self.nodes[0] 
        self.topmap=n.map.lca(n.p.map)

    # based on clusters
    def rfc(self,tree):

        def recclu(n):
            if hasattr(n,"sc"): return n.sc
            if n.leaf():
                n.clu=frozenset([n.label])
                n.sc=[ n.clu ]
            else:
                n.sc=sum((recclu(l) for l in n.c),[])
                n.clu=frozenset([])
                for l in n.c: n.clu=n.clu.union(l.clu)
                n.sc.append(n.clu)
            return n.sc

        self.setmap(tree)

        for n in self.nodes:
            n.rootingcluset=set(recclu(n)).union(recclu(n.p))
            n.rootingcluset.add(n.clu.union(n.p.clu))

        for l in tree.leaves: l.label=l.smplabel()
        r=set(recclu(tree.root))

        for n in self.nodes:
            n.urf=len(r.difference(n.rootingcluset))+len(n.rootingcluset.difference(r))
    
    def rdc(self,tree):
        self.setmap(tree)

        def pdist(a,b):
            # ugly
            cnt=0
            while a!=b:
                a=a.parent
                cnt+=1
            return cnt

        def dist(a,b):
            lc=a.lca(b)
            return pdist(a,lc)+pdist(b,lc)
           
        def dc(n):
            if hasattr(n,"dc"): return n.dc
            n.dc=sum(dc(l)+dist(l.map,n.map) for l in n.c)
            return n.dc

        for n in self.nodes:
            n.rdc=dc(n)+dc(n.p)+dist(n.map,n.p.map)

    def rf(self,tree):        
        self.setmap(tree)
        intu=self.start.isize()+self.start.p.isize()
        intr=len(tree.nodes)-len(tree.leaves)-1
        for n in self.nodes: n.rflca=0 if n.leaf() else -1
        for n in self.nodes:             
            n.urf=intu+intr-2*(n.getrflca()+n.p.getrflca())
        #self.pdeb()
                
    def pf(self,tree):
        self.rfc(tree)
        for n in self.nodes: n.cost=n.urf

        intu=self.start.isize()+self.start.p.isize()
        if intu<=2:            
            return "(" + self.start.lsubtree(tree)+","+self.start.p.lsubtree(tree)+")"
                
        lvs=[n for n in self.nodes if n.leaf() ]
        #print "#",lvs
        #for i in lvs:
        #    print i,i.p.maxdist()
        md=max(n.p.maxdist() for n in lvs)
        #print "#",md
        n=[ n for n in lvs if n.p.maxdist()==md ][0]
        
        res="("
        n=n.p
        n=[ x for x in n.c if not x.leaf() ][0]        
        res+=n.p.lsubtree(tree)+","

        while not n.leaf():
            md=max(c.maxdist() for c in n.c)
            if md==0: break            
            nx=[ x for x in n.c if md==x.maxdist() ][0]           
            res+="("+",".join(c.lsubtree(tree) for c in n.c if c!=nx)+")"+nx.linfo(tree,self.topmap,-1)+","
            n=nx            
        return res+n.lsubtree(tree)+")"
            
    def smprepr(self,tree,ct):     
        if ct=="RF":
            self.rfc(tree)
            for n in self.nodes: n.cost=n.urf
        elif ct=="DC":
            self.rdc(tree)
            for n in self.nodes: n.cost=n.rdc
        else:
            print "Unknown cost %s" % ct
            sys.exit(-1)

        

        return "("+self.start.lsubtree(tree,self.topmap)+","+self.start.p.lsubtree(tree,self.topmap)+")"
        
            
            

        

        
        
        
        
        
        
