import re


from traceback import print_stack


def smartints(v):
    if int(v)==v: return "%d.0"%v
    return "%f"%v

def pstack():
    print_stack()

def _tostr(s,mainlevel=1):

    def _tostrx(s):
        if type(s)==str: return  "\'"+s+"\'" if not re.search("^[0-9a-zA-Z]+$",s) else s
        if type(s)==tuple: return "("+",".join(map(_tostrx,s))+")"
        return s.__repr__()

    if type(s)==str: return  "\'"+s+"\'" if not re.search("^[0-9a-zA-Z]+$",s) else s
    if type(s)==tuple:
         return s[0]+"="+ _tostrx(s[1:] if len(s)>2 else s[1]) 

    return s.__repr__()

# input: list of directed edges, must be non-empty
# return: None if cycle or empty graph
#         top. sort otherwise



def sindex(a,val):
    try:
        return a.index(val)
    except ValueError:
        return -1

# math 
        
def totup(v):    
    if type(v)!=tuple: return (v,v)
    return v

def totup0(v):    
    if v and type(v)!=tuple:
        return (v,v)
    return v
        
        
def makelineabc(x1,y1,x2,y2):
    if x1==x2: return 1.0,0,-x1
    a=-(y1-y2)/(x1-x2)
    return a,1.0,-a*x1-y1


def point2lineprojection(x,y,a,b,c):
    return   x-(a*x+b*y+c)/(a*a+b*b)*a, y-(a*x+b*y+c)/(a*a+b*b)*b
    
    
    
                             
